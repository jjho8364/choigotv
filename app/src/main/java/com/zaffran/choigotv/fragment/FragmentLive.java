package com.zaffran.choigotv.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.zaffran.choigotv.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import static android.content.Context.MODE_PRIVATE;

public class FragmentLive extends Fragment implements View.OnClickListener {
    private final String TAG = " FragmentLive - ";
    private ListView listView;
    private String baseUrl = "";
    private ArrayList<String> pageUrlArr;
    private ArrayList<String> titleArr;

    // search
    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    ArrayAdapter<String> adapter;

    private String firstUrl = "";

    ArrayList<String> keyArr;
    JSONObject jsonObj;

    String m3u8Url = "";

    private String mxPlayerUrl = "market://details?id=com.mxtech.videoplayer.ad";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=1500;
    private long mLastClickTime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_tv, container, false);

        baseUrl = getArguments().getString("baseUrl");
        String strJson = baseUrl.replace("`", "\"");
        //Log.d(TAG, strJson);

        pageUrlArr = new ArrayList<String>();
        titleArr = new ArrayList<String>();

        try {
            jsonObj = new JSONObject(strJson);
            keyArr = new ArrayList<String>();

            Iterator<String> strKey = jsonObj.keys();
            while(strKey.hasNext()){
                keyArr.add(strKey.next().toString());
            }

            for(int i=0 ; i<keyArr.size() ; i++) {
                titleArr.add(keyArr.get(i));
                pageUrlArr.add(jsonObj.get(keyArr.get(i)).toString());

                //Log.d(TAG, keyArr.get(i) + " : " + jsonObj.get(keyArr.get(i)).toString());
            }

        } catch(Exception e){

        }

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "success to load AD");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "failed to load AD");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "closed AD");
                interstitialAd.loadAd(new AdRequest.Builder().build());

                checkMxplayerAndGo();

            }
        });

        listView = (ListView)view.findViewById(R.id.listview);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        if(getActivity() != null){
            if(titleArr.size() == 0){
                titleArr.clear();
                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                listView.setAdapter(adapter);
            } else {
                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;
                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            m3u8Url = pageUrlArr.get(position);

                            interstitialAd.show();

                        } else {
                            m3u8Url = pageUrlArr.get(position);

                            checkMxplayerAndGo();
                        }
                    }
                });
            }
        }


        return view;
    }

    public void checkMxplayerAndGo(){
        // cek mx player
        Intent startLink1 = getActivity().getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
        Intent startLink2 = getActivity().getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");
        if(startLink1 == null && startLink2 == null){
            // alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setTitle("필요한 플레이어")
                    .setMessage("재생을 위해 MX 플레이어가 필요합니다.")
                    .setPositiveButton("앱다운", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int value) {
                            //finish();
                            Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                            marketLaunch2.setData(Uri.parse(mxPlayerUrl));
                            startActivity(marketLaunch2);
                        }
                    })
                    .setNegativeButton("아니요", null);
            AlertDialog dialog = builder.create() ;
            dialog.show() ;

        } else {
            String packageName = "";

            Log.d(TAG, "m3u8Url : " + m3u8Url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri videoUri = Uri.parse(m3u8Url);
            intent.setDataAndType(videoUri, "application/x-mpegURL");
            intent.putExtra("decode_mode", (byte) 1);  // 1:sw, 2:hw
            intent.putExtra("video_zoom", 0);
            intent.putExtra("fast_mode", true);
            if (startLink2 != null) {
                packageName = "com.mxtech.videoplayer.pro";
            } else {
                packageName = "com.mxtech.videoplayer.ad";
            }
            intent.setPackage(packageName);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
