package com.zaffran.choigotv.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.zaffran.choigotv.R;
import com.zaffran.choigotv.adapter.GridViewAdapter;
import com.zaffran.choigotv.item.GridViewItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FakeFragment extends Fragment implements View.OnClickListener {
    private final String TAG = " FakeFragment - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private ArrayList<GridViewItem> gridArr;
    private GridViewAdapter adapter;
    private String baseUrl = "https://teddyzaffran.tistory.com/1";

    private ArrayList<String> pageUrlArr;
    private ArrayList<String> titleArr;
    private ArrayList<String> imageArr;

    SharedPreferences pref;
    String clickedUrl = "";

    private int adn = 0;
    private int adsCnt = 0;

    private GetListView getListView = null;

    //admob
    private InterstitialAd interstitialAd;
    AdRequest adRequest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fake_fragment, container, false);

        //baseUrl = getArguments().getString("baseUrl");
        Log.d(TAG, "come in here");
        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        gridView = (GridView)view.findViewById(R.id.gridview);

        adsCnt = 0;
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "success to load AD");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "failed to load AD");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "closed AD");
                interstitialAd.loadAd(new AdRequest.Builder().build());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
                startActivity(intent);
            }
        });
        //resetAds(interstitialAd);

        getListView = new GetListView();
        getListView.execute();

        return view;
    }

    public void resetAds(InterstitialAd interstitialAd){
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "success to load AD");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "failed to load AD");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "closed AD");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
                startActivity(intent);
            }
        });
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pageUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();
            imageArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            Document doc2 = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                String strHtml = doc.select("b").text();
                doc2 = Jsoup.parse(strHtml);

                Elements elements = doc2.select(".fakeList");

                Log.d(TAG, "size : " + elements.size());
                for(Element element: elements) {
                    String title = element.select(".title").text();
                    String imgUrl = element.select(".imgUrl").text();
                    String pageUrl = element.select(".pageUrl").text();

                    titleArr.add(title);
                    pageUrlArr.add(pageUrl);
                    imageArr.add(imgUrl);

                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "2222");
            if(getActivity() != null){
                gridArr = new ArrayList<GridViewItem>();

                for(int i=0 ; i<titleArr.size() ; i++){
                    gridArr.add(new GridViewItem(imageArr.get(i),titleArr.get(i),pageUrlArr.get(i)));
                }

                adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        clickedUrl = gridArr.get(position).getYutubUrl();
                        Log.d(TAG, "cliked : " + titleArr.get(position) + ", link : " + pageUrlArr.get(position));
                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            interstitialAd.show();
                            //adFull();

                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
                            startActivity(intent);
                        }

                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "fragment On paused");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "fragment onresume");
        //adRequest = new AdRequest.Builder().build();
        //interstitialAd = new InterstitialAd(getActivity());
        //interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        //interstitialAd.loadAd(adRequest);

        resetAds(interstitialAd);
    }

    public String getImgUrl(String fullStr) {
        String str = fullStr;
        String target1 = "(";
        String target2 = ")";
        int taget1Num = str.indexOf(target1);
        int taget2Num = str.indexOf(target2);

        return "https://" + str.substring(taget1Num+1, taget2Num);
    }
}
