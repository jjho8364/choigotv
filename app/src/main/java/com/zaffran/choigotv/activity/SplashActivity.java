package com.zaffran.choigotv.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zaffran.choigotv.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SplashActivity extends Activity {
    private String TAG = "SplashActivity - ";
    private GetStatus getStatus = null;
    private String splashInfo3 = "";
    private String tistoryUrl = "";
    private int tistoryLastPage = 0;
    private int tistoryRndPage = 0;
    private TextView tvSplashInfo;

    private String baseUrl = "";
    private String[] baseUrlArr = new String[]{
            "https://teddyzaffran.tistory.com/1"
            ,"https://findfoodbaru.tistory.com/1"
            //,"https://sherlong.tistory.com/1"
            //,"https://koreaskincare.tistory.com/1"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        tvSplashInfo = (TextView)findViewById(R.id.splash_info3);

        baseUrl = baseUrlArr[(int)(Math.random() * baseUrlArr.length)];

        Log.d(TAG, "selected baseUrl : " + baseUrl);

        getStatus = new GetStatus();
        getStatus.execute();
    }

    public class GetStatus extends AsyncTask<Void, Void, Void> {

        String appStatus = "";
        String fragment01Url = "";
        String fragment02Url = "";
        String fragment03Url = "";
        String fragment04Url = "";
        String fragMaruDrama2 = "";
        String fragMaruEnter2 = "";
        String fragMaruSearch = "";
        String fragBayMubi = "";
        String fragBayIld = "";
        String fragLinkDrama = "";
        String fragLinkEnter = "";
        String fragLinkMid = "";
        String fragLinkJoongd = "";
        String fragAtkorMubi = "";
        String fragAtkorMid = "";
        String fragAtkorJoongd = "";
        String fragAtkorIld = "";
        String fragKorMubiHan = "";
        String fragKorMubiOver = "";
        String fragKorMid = "";
        String fragKorCd = "";
        String fragKorJd = "";
        String fragAtkorMubiHanClassic = "";
        String fragAtkorMubiOverClassic = "";
        String fragLiveTv = "";
        String fragLiveTvEnter = "";
        String fragLiveTvSports = "";

        String maintenanceImgUrl = "";
        String closedImgUrl = "";
        String nextAppUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document tistoryDoc = null;
            Document doc = null;

            try {
                tistoryDoc = Jsoup.connect(baseUrl).timeout(15000).get();

                String strHtml = tistoryDoc.select("b").text();
                doc = Jsoup.parse(strHtml);

                appStatus = doc.select(".real").text();
                //appStatus = doc.select(".test").text();
                Log.d(TAG, "appStatus : " + appStatus);

                splashInfo3 = doc.select(".splash_info5").text();

                String[] tempStrCoder = doc.select(".coderstorage").text().split(",");
                tistoryUrl = tempStrCoder[0];
                tistoryLastPage = Integer.parseInt(tempStrCoder[1].trim());
                Log.d(TAG, "tistoryUrl : " + tistoryUrl);
                Log.d(TAG, "tistoryLastPage : " + tistoryLastPage);

                // 랜덤 페이지 가져오기
                tistoryRndPage = ((int)(Math.random() * (tistoryLastPage-1))) + 2;

                if(appStatus.equals("1")){
                    fragment01Url = doc.select(".fragment01url").text();
                    fragment02Url = doc.select(".fragment02url").text();
                    fragment03Url = doc.select(".fragment03url").text();
                    fragment04Url = doc.select(".fragment04url").text();
                    fragMaruDrama2 = doc.select(".fragMaruDrama2").text();
                    fragMaruEnter2 = doc.select(".fragMaruEnter2").text();
                    fragMaruSearch = doc.select(".fragMaruSearch").text();
                    fragBayMubi = doc.select(".fragmentMubiBay").text();
                    fragBayIld = doc.select(".fragmentMubiDongyoungsang").text();
                    fragLinkDrama = doc.select(".fragLinkDrama").text();
                    fragLinkEnter = doc.select(".fragLinkEnter").text();
                    fragLinkMid = doc.select(".fragLinkMid").text();
                    fragLinkJoongd = doc.select(".fragLinkJoongd").text();
                    fragAtkorMubi = doc.select(".fragAtkorMubi").text();
                    fragAtkorMid = doc.select(".fragAtkorMid").text();
                    fragAtkorJoongd = doc.select(".fragAtkorJongd").text();
                    fragAtkorIld = doc.select(".fragAtkorIld").text();
                    fragKorMubiHan = doc.select(".fragKorMubiHan").text();
                    fragKorMubiOver = doc.select(".fragKorMubiOver").text();
                    fragKorMid = doc.select(".fragKorMid").text();
                    fragKorCd = doc.select(".fragKorCd").text();
                    fragKorJd = doc.select(".fragKorJd").text();
                    fragAtkorMubiHanClassic = doc.select(".fragAtkorMubiHanClassic").text();
                    fragAtkorMubiOverClassic = doc.select(".fragAtkorMubiOverClassic").text();
                    fragLiveTv = doc.select(".livetv").text();
                    fragLiveTvEnter = doc.select(".livetventer").text();
                } else if(appStatus.equals("2")){
                    maintenanceImgUrl = doc.select(".maintenance").text();
                    fragment01Url = doc.select(".fragment01url").text();
                    fragment02Url = doc.select(".fragment02url").text();
                    fragment03Url = doc.select(".fragment03url").text();
                    fragment04Url = doc.select(".fragment04url").text();
                    fragMaruDrama2 = doc.select(".fragMaruDrama2").text();
                    fragMaruEnter2 = doc.select(".fragMaruEnter2").text();
                    fragMaruSearch = doc.select(".fragMaruSearch").text();
                    fragBayMubi = doc.select(".fragmentMubiBay").text();
                    fragBayIld = doc.select(".fragmentMubiDongyoungsang").text();
                    fragLinkDrama = doc.select(".fragLinkDrama").text();
                    fragLinkEnter = doc.select(".fragLinkEnter").text();
                    fragLinkMid = doc.select(".fragLinkMid").text();
                    fragLinkJoongd = doc.select(".fragLinkJoongd").text();
                    fragAtkorMubi = doc.select(".fragAtkorMubi").text();
                    fragAtkorMid = doc.select(".fragAtkorMid").text();
                    fragAtkorJoongd = doc.select(".fragAtkorJongd").text();
                    fragAtkorIld = doc.select(".fragAtkorIld").text();
                    fragKorMubiHan = doc.select(".fragKorMubiHan").text();
                    fragKorMubiOver = doc.select(".fragKorMubiOver").text();
                    fragKorMid = doc.select(".fragKorMid").text();
                    fragKorCd = doc.select(".fragKorCd").text();
                    fragKorJd = doc.select(".fragKorJd").text();;
                    fragAtkorMubiHanClassic = doc.select(".fragAtkorMubiHanClassic").text();
                    fragAtkorMubiOverClassic = doc.select(".fragAtkorMubiOverClassic").text();
                    fragLiveTv = doc.select(".livetv").text();
                    fragLiveTvEnter = doc.select(".livetventer").text();
                } else if(appStatus.equals("3")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".newappurl").text();
                } else if(appStatus.equals("9")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".mid.site").text();
                } else {
                    maintenanceImgUrl = doc.select(".maintenance").text();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tvSplashInfo.setText(splashInfo3);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplication(), MainActivity.class);

                    intent.putExtra("appStatus", appStatus);

                    if(appStatus.equals("1")){
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragMaruDrama2", fragMaruDrama2);
                        intent.putExtra("fragMaruEnter2", fragMaruEnter2);
                        intent.putExtra("fragMaruSearch", fragMaruSearch);
                        intent.putExtra("fragBayMubi", fragBayMubi);
                        intent.putExtra("fragBayIld", fragBayIld);
                        intent.putExtra("fragLinkDrama", fragLinkDrama);
                        intent.putExtra("fragLinkEnter", fragLinkEnter);
                        intent.putExtra("fragLinkMid", fragLinkMid);
                        intent.putExtra("fragLinkJoongd", fragLinkJoongd);
                        intent.putExtra("fragAtkorMubi", fragAtkorMubi);
                        intent.putExtra("fragAtkorMid", fragAtkorMid);
                        intent.putExtra("fragAtkorJoongd", fragAtkorJoongd);
                        intent.putExtra("fragAtkorIld", fragAtkorIld);
                        intent.putExtra("fragKorMubiHan", fragKorMubiHan);
                        intent.putExtra("fragKorMubiOver", fragKorMubiOver);
                        intent.putExtra("fragKorMid", fragKorMid);
                        intent.putExtra("fragKorCd", fragKorCd);
                        intent.putExtra("fragKorJd", fragKorJd);
                        intent.putExtra("fragAtkorMubiHanClassic", fragAtkorMubiHanClassic);
                        intent.putExtra("fragAtkorMubiOverClassic", fragAtkorMubiOverClassic);
                        intent.putExtra("fragLiveTv", fragLiveTv);
                        intent.putExtra("fragLiveTvEnter", fragLiveTvEnter);
                        intent.putExtra("fragLiveTvSports", fragLiveTvSports);
                        intent.putExtra("tistoryUrl", tistoryUrl);
                        intent.putExtra("tistoryRndPage", tistoryRndPage+"");

                    } else if(appStatus.equals("2")){
                        intent.putExtra("maintenance", maintenanceImgUrl);
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragMaruDrama2", fragMaruDrama2);
                        intent.putExtra("fragMaruEnter2", fragMaruEnter2);
                        intent.putExtra("fragMaruSearch", fragMaruSearch);
                        intent.putExtra("fragBayMubi", fragBayMubi);
                        intent.putExtra("fragBayIld", fragBayIld);
                        intent.putExtra("fragLinkDrama", fragLinkDrama);
                        intent.putExtra("fragLinkEnter", fragLinkEnter);
                        intent.putExtra("fragLinkMid", fragLinkMid);
                        intent.putExtra("fragLinkJoongd", fragLinkJoongd);
                        intent.putExtra("fragAtkorMubi", fragAtkorMubi);
                        intent.putExtra("fragAtkorMid", fragAtkorMid);
                        intent.putExtra("fragAtkorJoongd", fragAtkorJoongd);
                        intent.putExtra("fragAtkorIld", fragAtkorIld);
                        intent.putExtra("fragKorMubiHan", fragKorMubiHan);
                        intent.putExtra("fragKorMubiOver", fragKorMubiOver);
                        intent.putExtra("fragKorMid", fragKorMid);
                        intent.putExtra("fragKorCd", fragKorCd);
                        intent.putExtra("fragKofragmentCinarJd", fragKorJd);
                        intent.putExtra("fragAtkorMubiHanClassic", fragAtkorMubiHanClassic);
                        intent.putExtra("fragAtkorMubiOverClassic", fragAtkorMubiOverClassic);
                        intent.putExtra("fragLiveTv", fragLiveTv);
                        intent.putExtra("fragLiveTvEnter", fragLiveTvEnter);
                        intent.putExtra("fragLiveTvSports", fragLiveTvSports);
                        intent.putExtra("tistoryUrl", tistoryUrl);
                        intent.putExtra("tistoryRndPage", tistoryRndPage+"");
                    } else if(appStatus.equals("3")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else if(appStatus.equals("9")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else {
                        intent.putExtra("maintenance", maintenanceImgUrl);
                    }
                    finish();
                    startActivity(intent);
                }
            }, 3000);
        }
    }
}
